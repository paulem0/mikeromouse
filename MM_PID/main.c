/* MM_PID - an application for the Pololu 3pi Robot
 *
 * This application uses the Pololu AVR C/C++ Library.  For help, see:
 * -User's guide: http://www.pololu.com/docs/0J20
 * -Command reference: http://www.pololu.com/docs/0J18
 *
 * Created: 4/22/2016 9:00:35 PM
 *  Author: Michael Paule and Mike McCaffery
 * libpololu-avr in C drive
 * The .c files for atmel/pololu library stuff aren't in my C drive?(they are used from somewhere to toolchain points to?) So I look online?
 *
 * I believe that the Pololu library sets up the timers and the clock frequency and stuff? In OrangutanTime.c 
 
 
 * tester of the PID file/ how it will be implemented
 */
#include <pololu/3pi.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdbool.h>
#include <avr/wdt.h>
#include "defines.h"
//#include "ir_sensors.h"
//#include "motors.h"
#include "init.h"
#include "PID.h"
//#include "maze_solver.h"

//Mike M's
#include <pololu/orangutan.h>
#include <avr/io.h>
//Timer is at 3.2 microseconds between ticks


//prototypes for main.c are in defines.h so that PID can use the getters

char send_buffer[DATA_SIZE];

char encoder_flag1;
char encoder_flag2;
char motorFlag1;
char motorFlag2;

int encoder_right = 0;
int encoder_left = 0;

unsigned int timeNow = 0;

char encoder_right_data[DATA_SIZE];
char encoder_left_data[DATA_SIZE];

char STREAM_LEFT = 1;
char STREAM;


char encFlag = 1;
int main()
{
	init(); //run init.c

	
	
//	CollectData(250, 250); doesn't work TIMER ISR code is commented out
//	delay(1000);
	
	//@todo send the number of samples taken over 1 second (the timer ISR has trips the flag after 10000 ticks i.e. 1 second) over UART and see if the encoder ticks is good.
	
	set_motors(20, 0);	//4/28/16 at 11:37 we checked both more encoders spinning at top speed and they are exactly like the 6V pololu specs online //tested at 20, 1 tick every 8 ms
	while(encFlag){delay(1);};// we measured by hand that the timer ISR counted to 40k in 4 seconds, so Timer2 is at 10kHz right now
	set_motors(0,0);
	
//	move(100);
//	delay(1000);
//	move(100);
//	delay(1000);
//	move(400);
//	delay(500);
//	delay(1000);
//	move(444);
//	moveNPID(100);	
//	delay(1000);
//	moveNPID(1000);
	while(1){
	}
}

void CollectData(int m1, int m2){ // arguments motor 1 speed and motor 2 speed (-255 to +255) where 0 is full break // 255 may break our system though 
	encoder_flag1 = 1;
	motorFlag1 = 1;
	//serial_send_blocking("Motor1\n", 7);
	//sei();
//	delay(1000);

/* uncomment for right motor
	set_motors(SPEED_RIGHT_MOTOR, 0);
	while(motorFlag1){delay(1);};//PIDLR();}; 
	////serial_send_blocking("sending", 7);
	SendData(encoder_left_data);
*/
// uncomment for left motor
	encoder_flag2 = 1;
	motorFlag2 = 1;
	//serial_send_blocking("Motor2", 6);
	//serial_send_blocking("!", 1);
	set_motors(SPEED_LEFT_MOTOR, 0);
	while(motorFlag2){delay(1);};//PIDLR();};
	SendData(encoder_right_data);
	//cli();
}

void SendData(char *data){
	unsigned int i;
	for (i = 0; i < DATA_SIZE; i++){
		while(!(UCSR0A & (1 << UDRE0)));
		UDR0 = *data;
		data++;
	}

}

/*
*/
char getLeftEncoderTicks(){
	return encoder_left;
}

char getRightEncoderTicks(){
	return encoder_right;
}

void reset_encoders(){
	encoder_left = 0;
	encoder_right = 0;
}

void sendEncoders(){
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = 0xFE;
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = encoder_left>>8;
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = encoder_left;
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = 0xFE;
//	while(!(UCSR0A & (1 << UDRE0)));
//	UDR0 = encoder_right;
	}

/*
*Dillon's move function modified
*
*/
void move(int dist){
	
	reset_encoders();
	sendEncoders();
	set_motors(SPEED_LEFT_MOTOR, SPEED_RIGHT_MOTOR);
	delay(1);
	TCNT0 = 0; //reset the timer interupt for sampling
	STREAM = 1;
	
	while(encoder_left < dist){		//encoder_left+encoder_right)/2
		delay(1);
		sendEncoders();
	//	PIDLR();
	}
	set_motors(0,0);		
	//UDR0 = 0xFF;
	STREAM = 0;
}



void moveNPID(int dist){
	reset_encoders();
	set_motors(SPEED_LEFT_MOTOR, SPEED_RIGHT_MOTOR);
	delay(1);
	while((encoder_left+encoder_right)/2 < dist){
		delay(1);
	}
	set_motors(0,0);
}



//ISRs start now as follows

ISR (PCINT0_vect) // pin change interrupt 0 // right encoder
{
	//static unsigned int leftlimit = TICKDIVIDERLEFT - 1;
	if (PINB & (1 << PINB1)){// && !leftlimit){
			encoder_right++;
			if (STREAM_LEFT && STREAM){
				while(!(UCSR0A & (1 << UDRE0)));
//				UDR0 = timeNow;
				timeNow = 0;
			}
			//leftlimit = TICKDIVIDERLEFT;
		}
		//leftlimit--;
		
}	


ISR (PCINT1_vect) // pin change interrupt 1		//left encoder
{
//	static unsigned int leftlimit = TICKDIVIDERLEFT - 1;
	if (PINC & (1<< PINC5)){// && !leftlimit){
		encoder_left++;
		
		if (STREAM_LEFT && STREAM){
			while(!(UCSR0A & (1 << UDRE0)));
			//				UDR0 = timeNow;
			timeNow = 0;
		}
		//leftlimit = TICKDIVIDERLEFT;
	}
	//leftlimit--;
	PCIFR |= (1 << PCIF1);
}

ISR (TIMER2_COMPA_vect)  // timer0 compare interrupt
{
	static unsigned int DIVIDE = DIVIDER - 1;
	
/*
	static unsigned int i = 0; // left index
	static unsigned int j = 0; // right index
	



	if(!DIVIDE){

		if (i < DATA_SIZE && encoder_flag1){
			encoder_left_data[i++] = encoder_left; // acquire left encoder data, increment index
			//serial_send_blocking(&i, 1);
//			encoder_left = 0;
		}

		if (j < DATA_SIZE && encoder_flag2){
			encoder_right_data[j++] = encoder_right; // acquire right encoder data, increment index
			//serial_send_blocking(&encoder_right, 1);
			//serial_send_blocking("!", 1);
//			encoder_right = 0;
		}
		DIVIDE = DIVIDER;
		//serial_send_blocking(&i, 1);
		//serial_send_blocking(&j, 1);
	}

	//serial_send_blocking(&i, 1);
	if (i >= DATA_SIZE && motorFlag1){
		set_motors(0,0);
		motorFlag1 = 0;
		//serial_send_blocking("switch", 6);
	}
	//serial_send_blocking(&j, 1);
	if (j >= DATA_SIZE && motorFlag2){
		set_motors(0,0);
		motorFlag2 = 0;
	}
	*/
if(timeNow > 10000)
{
	encFlag = 0;
}
timeNow++;

/*if(!DIVIDE){
		
	
	//
	//if(STREAM){ // use this as global variable to control when you are sending data, 1 = stream, 0 = don't stream
		//DIVIDE = DIVIDER;
		//
		//while(!(UCSR0A & (1 << UDRE0)));
		//if(STREAM_LEFT) {
			//temp = (char)(encoder_left-encodeLeftPrev);
			//if(temp>127) {	//then it was actually a negative value because we are expecting 0 to 10 and unsigned neg is above 127 (that neg bit is high)
				//temp = -temp;
			//}
			//UDR0 = temp; // STREAM_LEFT chooses which motor to get data from. 1 = left, 0 = right
		//}
		//else {
			//temp = (char)(encoder_right-encodeRightPrev);
			//if(temp>127) {	//then it was actually a negative value because we are expecting 0 to 10 and unsigned neg is above 127 (that neg bit is high)
				//temp = -temp;
			//}
			//UDR0 = temp;
		//}
		//encodeLeftPrev = encoder_left;
		//encodeRightPrev = encoder_right;

	}
*/
	TIFR0 |= (1 << OCF0A);
	DIVIDE--;

}


