/************************************************************************/
/* 3PI -- Motor encoder over UART
/* BAUD rate = 500,000
/* Set up for live transmission of encoder data
/* Author : Michael McCaffrey 
/* 4/22/2016 
/************************************************************************/

#include <pololu/orangutan.h>
#include <avr/io.h>
#include <avr/interrupt.h>

unsigned char encoder_right = 0;
unsigned char encoder_left = 0;

int main()                 
{
	cli(); // clear all interrupt vectors

	// GPIO
	DDRB &= ~(1 << DDB1); // set PB1 input
	DDRC &= ~(1 << DDC5);  // set PC5 as input	
	
	// GPIO interrupts
	PCICR = (1 << PCIE0) | (1 << PCIE1); // enable interrupts on pin change 1 and 2
	PCMSK0 = 1 << PCINT1; // enable pin PB1 interrupts
	PCMSK1 = (1 << PCINT13); // enable pin PC5 for interrupts  
	EICRA |= (1 << ISC11); // rising edge pin change interrupts
	EIMSK |= (1 << INT0) | (1 << INT1); // external interrupt mask 	
	
	// timer 
    TCCR0A |= (1 << WGM01); // clear timer on compare
	TCCR0B |= (1 << CS01); // set pre-scaler = 16
    OCR0A = 100; // set compare value to 100
    TIMSK0 |= (1 << OCIE0A); // timer compare mask
	
	// UART pins
	DDRD |= 1 << DDD1; // set PD1 input
	DDRD *= ~(1 << DDD0); // set PD0 output
	
	// UART init
	UBRR0H = 0; // bitrate high register baud rate 0.5M
	UBRR0L = 0; // bitrate low register
	UCSR0B = 1 << TXEN0; // UART TX enable
	UCSR0B = 1 << RXEN0; // UART RX enable
	UCSR0C = 0x03 << UCSZ00; // 8 bit, no parity, 1 stop bit 

    sei(); //enable interrupts

	set_motors(0); // stop motors 
	
	// countdown for transmission
	unsigned char count = 3;
	while(count){
		lcd_goto_xy(0, 0);
		print(&count);
		count--;
		delay(1000);
	}	
	
	int motorSpeed = 127; // set duty cycle to (127/255) 
	set_motors(motorSpeed, motorSpeed);  // set speeds of motors 1 and 2

	while(1){}

}
ISR (PCINT0_vect) // pin change interrupt 0
{	
	// increase count for right motor encoder pulse on PB1 and send over UART
	if (PINB & (1 << PINB1)){
		encoder_right++;
	}
	
}

ISR (PCINT1_vect) // pin change interrupt 1
{
	
	// increase count for left motor encoder pulse on PC5
	if (PINC & (1<< PINC5)){
		encoder_left++; 
	}
	
}


ISR (TIMER0_COMPA_vect)  // timer0 overflow interrupt
{
	while (!(UCSR0A & (1 << UDRE0))); // wait for empty UART data register
	UDR0 = encoder_left;

	while (!(UCSR0A & (1 << UDRE0))); // wait for empty UART data register
	UDR0 = encoder_left;
}