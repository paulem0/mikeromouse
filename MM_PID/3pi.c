/*
 * _3pi.c
 *
 * Created: 4/25/2016 10:32:09 PM
 *  Author: Michael Paule and Michael McCaffrey
 */ 

#ifdef _TEST_

/***********************************************************************
/ 3PI -- Motor encoder over UART
/ BAUD rate = 500,000
/ Set up for storage and dump of encoder data
/ Author : Michael McCaffrey
/ 4/22/2016
/***********************************************************************/

#include <pololu/orangutan.h>
#include <avr/io.h>
#include "defines.h"

#define LEFT

#define FCPU 20000000
#define BAUDRATE 28800
#define BAUD_PRESCALLER (((FCPU / (BAUDRATE * 16))) - 1)
#define DATA_SIZE 650
#define DIVIDER 8

char send_buffer[DATA_SIZE];

char encoder_flag1;
char encoder_flag2;
char motorFlag1;
char motorFlag2;

char encoder_right = 0;
char encoder_left = 0;

char encoder_right_data[DATA_SIZE];
char encoder_left_data[DATA_SIZE];

//void MotorCountdown();
//void CollectData(int m1, int m2);
//void SendData(char data[]);

int main()
{

serial_set_baud_rate(BAUDRATE);
// GPIO
DDRB &= ~(1 << DDB2); // set PB1 input
DDRC &= ~(1 << DDC5);  // set PC5 as input

// GPIO interrupts
PCICR = (1 << PCIE0) | (1 << PCIE1); // enable interrupts on pin change 1 and 2
//serial_send_blocking("Enable int", 10);
PCMSK0 = 1 << PCINT2; // enable pin PB2 interrupts
PCMSK1 = (1 << PCINT13); // enable pin PC5 for interrupts
//serial_send_blocking("Check", 5);
// timer
TCCR0A |= (1 << WGM01); // clear timer on compare
TCCR0B |= (1 << CS02) | (1 << CS00); // set pre-scaler = 8
OCR0A = 250; // set compare value to 250
TIMSK0 |= (1 << OCIE0A); // timer compare mask

// UART pins
DDRD |= 1 << DDD1; // set PD1 input
DDRD *= ~(1 << DDD0); // set PD0 output

// UART init
UCSR0B = (1<<RXEN0)|(1<<TXEN0); // enable UART tx, rx
UCSR0C = 0x03 << UCSZ00; // 8 bit, no parity, 1 stop bit
//serial_send_blocking("Initialized", 11);
//serial_send_blocking("Counted Down", 12);
//SendData(encoder_right_data);
//while(1){};
CollectData(250, 250);

while(1){
}
}

void CollectData(int m1, int m2){ // arguments motor 1 speed and motor 2 speed (-255 to +255)
encoder_flag1 = 1;
motorFlag1 = 1;
//serial_send_blocking("Motor1\n", 7);
//sei();
set_motors(m1, 0);
while(motorFlag1){delay(1);};
////serial_send_blocking("sending", 7);
SendData(encoder_left_data);
encoder_flag2 = 1;
motorFlag2 = 1;
//serial_send_blocking("Motor2", 6);
//serial_send_blocking("!", 1);
set_motors(0, m2);
while(motorFlag2){delay(1);};
SendData(encoder_right_data);
//cli();
}

void SendData(char *data){
unsigned int i;
for (i = 0; i < DATA_SIZE; i++){
while(!(UCSR0A & (1 << UDRE0)));
UDR0 = *data;
data++;
}

}

ISR (PCINT0_vect) // pin change interrupt 0
{
if (PINB & (1 << PINB1)){
encoder_right++;
}
PCIFR |= (1 << PCIF0);
}

ISR (PCINT1_vect) // pin change interrupt 1
{
if (PINC & (1<< PINC5)){
encoder_left++;
}
PCIFR |= (1 << PCIF1);
}

ISR (TIMER0_COMPA_vect)  // timer0 compare interrupt
{
static unsigned int i = 0; // left index
static unsigned int j = 0; // right index
static unsigned char DIVIDE = DIVIDER - 1;

if(!DIVIDE){

if (i < DATA_SIZE && encoder_flag1){
encoder_left_data[i++] = encoder_left; // acquire left encoder data, increment index
//serial_send_blocking(&i, 1);
encoder_left = 0;
}

if (j < DATA_SIZE && encoder_flag2){
encoder_right_data[j++] = encoder_right; // acquire right encoder data, increment index
//serial_send_blocking(&encoder_right, 1);
//serial_send_blocking("!", 1);
encoder_right = 0;
}
DIVIDE = DIVIDER;
//serial_send_blocking(&i, 1);
//serial_send_blocking(&j, 1);
}

//serial_send_blocking(&i, 1);
if (i >= DATA_SIZE && motorFlag1){
set_motors(0,0);
motorFlag1 = 0;
//serial_send_blocking("switch", 6);
}
//serial_send_blocking(&j, 1);
if (j >= DATA_SIZE && motorFlag2){
set_motors(0,0);
motorFlag2 = 0;
}

TIFR0 |= (1 << OCF0A);
DIVIDE--;

}

#endif