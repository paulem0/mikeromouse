/*
 * Init.h
 *
 * Created: 4/22/2016 10:39:31 PM
 *  Author: Michael P
 */ 


#ifndef INIT_H_
#define INIT_H_


/**
 * @brief Initializes everything
 */
void init(void);


#endif /* INIT_H_ */