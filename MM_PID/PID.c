/*
 * PID.c
 *
 * Created: 4/22/2016 10:06:59 PM
 *  Author: Michael Paule
 
 This is based off of Dillon's code right now, but it is using the encoders as the error sources
 */ 
#include <pololu/3pi.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "defines.h"
#include "init.h"

uint16_t left;
uint16_t right;
uint16_t front;
uint8_t SETPOINT = 0;

int ir_diff = 0;
int error = 0;
int prevError = 0;
float pid = 0;

float dt = 0;
float ticks = 0;
float prevTicks = 0;
float accum_error = 0;
float xP = 0;
float xI = 0;
float xD = 0;

float Kp = 12.5/10; // 12.5
float Ki = 5/10; // 5
float Kd = 12.5/10; // 12.5



void resetPID(void){
	accum_error = 0;
}

void PIDLR(void){
	//compare encoder values
	left = getLeftEncoderTicks();		//@todo add Mike's encoder library
	right = getRightEncoderTicks();
	error = right - left;		//error in motor encoder ticks - the future values may need to be altered based on how these ticks ad or subtract from the motor speed
	
	//if(ir_diff != 0 || ir_diff != 65000){ // if there is at least one wall OR not an jitter in calculating error
		// Calculate PID
		
		// Calculate dt
		ticks = get_ticks(); //time between last measurement
		dt = ticks_to_microseconds(ticks - prevTicks) / 1000000.0;	//@todo FCPU is 10000000 for them?
		prevTicks = ticks;

		// Calculate P term
		xP = Kp * error;
		
		// Calculate I term
		accum_error += (error * dt);
		xI = Ki * accum_error;

		// Calculate the D
		xD = Kd * ((error - prevError) / dt);
		
		prevError = error;
		
		pid = ((xP + xI + xD) / 100000.0);	//curious of why a thousand? It must have something to do with the motor speeds from -255 to 255?
		pid = 0;
		//update motors
		speedLeft = SPEED_LEFT_MOTOR - pid / 2;
		speedRight = SPEED_RIGHT_MOTOR + pid / 2;
		
		set_motors(speedLeft, speedRight);
//		}else{
//		//normalizeMotors();
//		set_motors(SPEED_LEFT_MOTOR - MOTOR_OFFSET, SPEED_RIGHT_MOTOR + MOTOR_OFFSET);
//	}
}


