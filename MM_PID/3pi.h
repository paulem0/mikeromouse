/*
 * _3pi.h
 *
 * Created: 4/25/2016 10:32:23 PM
 *  Author: Michael Paule and Michael McCaffrey
 */ 


#ifndef 3PI_H_
#define 3PI_H_

#ifdef _TEST_
void MotorCountdown();
void CollectData(int m1, int m2);
void SendData(char data[]);

#endif

#endif /* 3PI_H_ */