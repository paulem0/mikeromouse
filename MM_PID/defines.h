/*
 * defines.h
 *
 * Created: 4/22/2016 9:51:16 PM
 *  Author: Michael P
 */ 
#include <stdbool.h>

#ifndef DEFINES_H_
#define DEFINES_H_

// just using a distance in the move function #define BLOCK 400//1752 /**< was 1840 */
#define TURN_ENCODER 652 /**< amount of ticks it takes to turn 90 degrees */
#define TURN_SPEED 35 /**< 15/128 speed */
#define SPEED_LEFT_MOTOR 50		//PWM is 100%  of 5V forward (255/255)				/50				//100 = 4.5 , 80 = 4.3, 60 = 3.8, 40 = 3.2, 20 = 
#define SPEED_RIGHT_MOTOR 50		//50			//20 = 4.3
#define SPEED_CALIBRATION 20 /**< speed mouse is at in calibrate function */


#define ENCODER_THRESHOLD 20 //PID says that the motors are the same if the ticks are within 20 ticks @todo


#define BLOCK_THRESHOLD 325 /**< minimum number of encoder ticks */
#define MOTOR_OFFSET 0
#define HIGH 1
#define LOW 0


volatile int8_t speedLeft;		//the speeds that the motors are currently running at, set every time the motor speeds are changed
volatile int8_t speedRight;


//Mike M's defines
#define LEFT

#define FCPU 20000000
#define BAUDRATE 28800
#define BAUD_PRESCALLER (((FCPU / (BAUDRATE * 16))) - 1)
#define DATA_SIZE 100
#define DIVIDER 50//64


#define TICKDIVIDERLEFT 1

//prototypes for main.c

void CollectData(int m1, int m2);
void SendData(char data[]);
char getLeftEncoderTicks(void);
char getRightEncoderTicks(void);
void reset_encoders(void);
void move(int dist);
void moveNPID(int dist);


#endif /* DEFINES_H_ */