/*
 * PID.h
 *
 * Created: 4/22/2016 9:32:03 PM
 *  Author: Michael P
 *
 * This is the PID controller that is used to control the control the bot going straight (PIDLR) and accelerating and decelerating (PIDACL)
 *
 */ 


#ifndef PID_H_
#define PID_H_

/** 
 * @brief Resets PID
 *
 * Resets accumulation error
 */
void resetPID(void);

/** 
 * @brief Resets PID
 *
 * Resets accumulation error
 */
void PIDLR(void);



#endif /* PID_H_ */