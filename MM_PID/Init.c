/*
 * Init.c
 *
 * Created: 4/22/2016 10:13:10 PM
 *  Author: mpspa
 */ 

/*
 * init.c
 *
 * Created: 3/31/2014 11:11:32 PM
 *  Author: Dillon
 */ 

#include <pololu/3pi.h>
#include <stdio.h>
#include "defines.h"

// Init functions
void init(){

	DDRD &= ~(1 << PORTD0);// Set pin PD0 as an input
	PORTD |= 1 << PORTD0;// Enable pull-up on pin PD0 so that it isn't floating
	DDRB &= ~(1 << PORTB2);// Set pin PB2 as an input
	PORTB |= 1 << PORTB2;// Enable pull-up on pin PB2 so that it isn't floating

	PCICR = 0x05;	// Enable pin-change interrupt for masked pins of PORTD
	PCIFR = 0x07;	// Clear all pin-change interrupt flags
	PCMSK2 = 0x01;	// Set pin-change interrupt mask for pin PD0
	PCMSK0 = 0x04;	// Set pin-change interrupt mask for pin PB3
	sei();	// Interrupts are off by default so enable them

	set_analog_mode(MODE_10_BIT);
	
	// Set motor speeds
	speedLeft = SPEED_LEFT_MOTOR;
	speedRight = SPEED_RIGHT_MOTOR;
	clear(); //LCL screen clear
	

	//Mike M's Initialization of encoder pins, 
	serial_set_baud_rate(BAUDRATE);
	// GPIO
	DDRB &= ~(1 << DDB2);  // set PB1 input			Left encoder's pin
	DDRC &= ~(1 << DDC5);  // set PC5 as input		Right encoder's pin
	
	// GPIO interrupts (for encoders)
	PCICR = (1 << PCIE0) | (1 << PCIE1); // enable interrupts on pin change 1 and 2
	//serial_send_blocking("Enable int", 10);
	PCMSK0 = 1 << PCINT2; // enable pin PB2 interrupts
	PCMSK1 = (1 << PCINT13); // enable pin PC5 for interrupts
	//serial_send_blocking("Check", 5);

	// timer (for collecting data...
	TCCR2A |= (1 << WGM01) | (1<<COM0A0); // clear timer on compare
	TCCR2B |= (1 << CS01) | (1 << CS00); // set pre-scaler = 8
	OCR2A = 0; // set compare value to 250
	TIMSK2 |= (1 << OCIE0A); // timer compare mask

	// UART pins
	DDRD |= 1 << DDD1; // set PD1 input
	DDRD *= ~(1 << DDD0); // set PD0 output

	// UART init
	UCSR0B = (1<<RXEN0)|(1<<TXEN0); // enable UART tx, rx
	UCSR0C = 0x03 << UCSZ00; // 8 bit, no parity, 1 stop bit
	//serial_send_blocking("Initialized", 11);
	
	
}