/**
 * @file control.h
 * @author Dillon Buck, Samed Ozdemir, Matt Austen
 * @date 14 Apr 2014
 * @brief Implements control and recalibration to mouse
 */

#ifndef CONTROL_H_
#define CONTROL_H_

/**
 * @brief Resets PID
 *
 * Resets accumulation error
 */
void resetPID(void);

/**
 * @brief PID controller
 *
 * This function implemented a PID controller to keep the micromouse
 * driving straight.
 */
void PID2(void);

/**
 * @brief Measures distances between walls
 *
 * This function identifies the mouse's distance from the walls by
 * comparing the distances between the mouse and each of the sensors.
 */
void recalibrate(uint8_t walls);

/**
 * @brief Recalibrates distance from walls
 *
 * This function recalibrates the mouse's distance from the walls by
 * comparing the distances between the mouse and each of the sensors.
 */
void recalibrate2(void);

#endif /* CONTROL_H_ */