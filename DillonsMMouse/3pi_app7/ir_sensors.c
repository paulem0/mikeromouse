/*
 * ir_sensors.c
 *
 * Created: 3/31/2014 11:33:40 PM
 *  Author: Dillon
 */ 

#include <pololu/3pi.h>
#include "defines.h"
#include "ir_sensors.h"

// Used to store the value read from the IR Sensors
uint32_t x;
uint8_t i;

// Averages the IR sensors on both sides (left and right) to determine the center of the block
uint16_t avg_sides(){
	uint32_t AvgT = 0;	// temporary average
	uint16_t Avg = 0;	// overall average distance the mouse should be from walls	
	
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn IR LEDs on.
	delay(1);	//Allow time for LED to turn on
	// Calculate average distance to walls 
	for(i=0; i < 200; i++){
		AvgT += (analog_read(5) + analog_read(7))/2;
	}
	
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off LEDs.
	
	Avg = AvgT / 200; // find the average of the left and right side
	
	return Avg;
}
//
//// Used for debugging the sensors in the front of the micro-mouse (message is printed to screen)
//void debugF(){
	//clear();
	//set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	//print_long(analog_read(6));
	//lcd_goto_xy(0,1);
	//print_long(analog_read(1));
	//set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
//}
//
//// Used for debugging all the sensors (message is printed to screen)
//// depreciated (more sensors were added making this function less useful
//void debugSensors(){
	//clear();
	//print_long(readFrontLeft());
	//lcd_goto_xy(5, 0);
	//print_long(readFrontRight());
	//lcd_goto_xy(0,1);
	//print_long(readLeft());
	//lcd_goto_xy(5,1);
	//print_long(readRight());
//}

// Read and return the value of the front left IR sensor
uint16_t readFrontLeft(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = analog_read(3);
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read and return the value of the front right IR sensor
uint16_t readFrontRight(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = analog_read(5);
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read both front IR sensors and return the average value
uint16_t readFront(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = (analog_read(3) + analog_read(5)) / 2;
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read the left IR sensor closer to the front
uint16_t readLeftFront(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = (analog_read(0));
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read the left IR sensor closer to the back
uint16_t readLeftBack(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = (analog_read(1));
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read and return the average values of the left IR sensors
uint16_t readLeft(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = (analog_read(0) + analog_read(1)) / 2;
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read the left IR sensor closer to the front
uint16_t readRightFront(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = (analog_read(6));
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

// Read the left IR sensor closer to the back
uint16_t readRightBack(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = analog_read(7);
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}

/*
// Read and return the average values of the left IR sensors
uint16_t readRight(void){
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(1);
	x = (int) (analog_read(6) + analog_read(7)) / 2;
	set_digital_output(IO_D1, LOW); // PC1 is low to turn off IR LEDs
	return x;
}
*/

// Record the left IR sensor closer to the front when the micro-mouse is in the middle of the block at start.
uint16_t seedLeftFront(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += analog_read(0);
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
	
	return x/200;
}

// Record the left IR sensor closer to the back when the micro-mouse is in the middle of the block at start.
uint16_t seedLeftBack(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += analog_read(1);
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
	
	return x/200;
}

// Record the right IR sensor closer to the front when the micro-mouse is in the middle of the block at start.
uint16_t seedRightFront(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += analog_read(6);
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
		
	return x/200;
}

// Record the right IR sensor closer to the back when the micro-mouse is in the middle of the block at start.
uint16_t seedRightBack(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += analog_read(7);
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
	return x/200;
}

// Record the front left IR sensor when the micro-mouse is in the middle of the block at start.
uint16_t seedFrontLeft(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += analog_read(6);
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
	
	return x/200;
}

// Record the front left IR sensor when the micro-mouse is in the middle of the block at start.
uint16_t seedFrontRight(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += analog_read(1);
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
	
	return x/200;
}

// Record the average value of both front IR sensors.
uint16_t seedFront(void){
	x=0;
	set_digital_output(IO_D1, HIGH); // PC1 is high to turn on IR LEDs
	delay(50);
	for(i=0; i<200; i++){
		x += (analog_read(3) + analog_read(5)) / 2;
	}
	set_digital_output(IO_D1, LOW); // PC1 is high to turn on IR LEDs
	
	return x/200;
}

// Used for debugging sensors on the left and right side (message is printed to screen)
void debugLR(){
	clear();
	print_long(readLeftBack());
	lcd_goto_xy(0, 1);
	print_long(readRightBack());
}

// Checks for the difference in the IR sensors on the left and right side
int checkDistance(void){
	int ir_diff = 0;
	uint16_t left = readLeftBack();
	uint16_t right = readRightBack();
	
		// If there is no wall on either side, use encoder data and set ir_diff = 0
		if(left < IR_LEFT_THRESHOLD && right < IR_RIGHT_THRESHOLD){
			ir_diff = 0;
		}
		else if(left < IR_LEFT_THRESHOLD){ // if there is only a right wall
			ir_diff = 2 * (IR_RIGHT_CENTER - right);
		}
		else if(right < IR_RIGHT_THRESHOLD){ // if there is only a left wall
			ir_diff = 2 * (left - IR_LEFT_CENTER);
		}else{ // both walls are present
			ir_diff = (left - right);
		}
	
	return ir_diff;
}

// Check left, front, and right side of micro-mouse for a wall. If there is a wall there, set the bit high. The wall behind the micro-mouse is assumed to be 0 at all times.
// Returns uint8_t containing wall information in the form of XXXXURDL (up, right, down, left).
uint8_t checkWalls(){
	uint8_t walls = 0;
	
	// Check for walls function
	
	//Read wall sensors
	//up wall
	if(readFront() > IR_FRONT_THRESHOLD){
		walls |= (1 << 3);
	}else{
		walls &= ~(1 << 3);
	}
	
	// left wall
	if(readLeftBack() > IR_LEFT_THRESHOLD){
		walls |= (1 << 0);
	}else{
		walls &= ~(1 << 0);
	}
	// right wall
	if(readRightBack() > IR_RIGHT_THRESHOLD){
		walls |= (1 << 2);
		}else{
		walls &= ~(1 << 2);
	}
	
	// down wall (always assumed to be no wall)
	walls &= ~(1 << 1);
	
	return walls;
}