/**
*
* @mainpage Project Description
* @authors Dillon Buck, Samed Ozdemir, Matt Austen

* For our project we built a micromouse from a Polulu 3pi base.  It was
* built with the intent of participating in the 2014 SAC competition.  The
* goal of the micromouse competition is to create a robot that navigates a
* 16x16 maze (each unit is 18cmx18cm) starting in a corner an locating the
* center.  The center is comprised of four blocks.  The mouse that got to 
* center of the maze first won the competition.  The micromouse competition
* consisted of two separate competitions:  the kit competition and the 
* scratch competition.  In the scratch competition, the mouse is built
* from scratch.  We participated in the kit competition, where the mouse
* is constructed from a base.  This saves considerable time in preparing 
* the hardware, while still providing a challenge in developing the
* software.
* 
* In general, the mouse needs to be designed to move quickly without
* draining so much power that it cannot make it to the center of the
* maze.  The power source for the mouse was 4 AA batteries.  It also needs
* to be able to map out the maze internal and keep track of where it is, 
* where it has been, and make decisions on where it needs to go next.  The
* mouse was controlled my an Atmega328P.  All programming for the mouse was
* done in C and in Atmel Studio.
* 
* Another hardware challenge was to reliably track the distance that the 
* mouse has travelled.  Since we know the dimensions of the maze and each 
* "cell" of the maze, it is crucial that the mouse track distance precisely.  
* This problem was solved using wheel encoders.  The DC motors turned
* with a 100:1 ratio.  In other words, for each turn of the motor the
* encoders turned 100 times.  The encoders had 5 ticks each, so each encoder
* turn was 5 ticks.  With each encoder turn we can assign a distance.  As
* long as each tick is acknowledged accurately, the distance that the mouse
* travels can be recorded accurately.  
* 
* To detect walls we used IR sensors, which return a voltage depending on 
* how far away from a wall the sensor is.  The mouse consistently checked 
* if there was a wall.  Whenever it reached an area with two or more walls,
* it recalibrated its position to ensure that it did not get thrown off 
* later.  The mouse moved at about 40% of its maximum speed so that it was
* able to keep track of distance accurately.  To keep the mouse going straight,
* a PID controller was implemented. 
* 
* The code was written so that the micromouse could map a maze of a size that 
* was chosen by the user. Since the competition relied on the fastest time to 
* get to the center, the Micromouse used the Flood Fill algorithm to try and r
* each the center first. Once at the center, the code switched into Dijkstra’s 
* Algorithm to map the entire maze and go the beginning coordinates. Once at the 
* beginning coordinates, the Micromouse calculates the shortest path to the center 
* and solves again. The benefit of this is that, if the time to get to the center 
* is shorter than the time the Flood Fill algorithm took, the shorter time would 
* be ruled as the official time for the competition. 
 */