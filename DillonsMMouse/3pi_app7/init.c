/*
 * init.c
 *
 * Created: 3/31/2014 11:11:32 PM
 *  Author: Dillon
 */ 

#include <pololu/3pi.h>
#include <stdio.h>
#include "defines.h"

// Init functions
void init(){
	DDRD &= ~(1 << PORTD0);// Set pin PD0 as an input
	PORTD |= 1 << PORTD0;// Enable pull-up on pin PD0 so that it isn't floating
	DDRB &= ~(1 << PORTB2);// Set pin PB2 as an input
	PORTB |= 1 << PORTB2;// Enable pull-up on pin PB2 so that it isn't floating

	PCICR = 0x05;	// Enable pin-change interrupt for masked pins of PORTD
	PCIFR = 0x07;	// Clear all pin-change interrupt flags
	PCMSK2 = 0x01;	// Set pin-change interrupt mask for pin PD0
	PCMSK0 = 0x04;	// Set pin-change interrupt mask for pin PB3
	sei();	// Interrupts are off by default so enable them

	set_analog_mode(MODE_10_BIT);
	
	// Set motor speeds
	speedLeft = SPEED_LEFT_MOTOR;
	speedRight = SPEED_RIGHT_MOTOR;
	clear();
}


