/**
* @file motors.h
* @author Dillon Buck, Samed Ozdemir, Matt Austen
* @date 14 Apr 2014
* @brief File containing functions for motors. Contains the move and turn function.
*/

#include "defines.h"

/**
* @brief Turns the mouse, using the encoders to precisely calculate the degree of the turn.
* Turn to the direction that Chris algorithm tells us 0:no turn. 1: turn right. 2: turn around. 3: turn left
*/
void turn(uint8_t dir);

/**
* @brief Moves the mouse forward 1 block. Also calculated using encoder counts. 
*/
void move(void);

/** 
* @brief Resets the encoder values to 0. This is usually called after a turn.
*/
void reset_encoders(void);