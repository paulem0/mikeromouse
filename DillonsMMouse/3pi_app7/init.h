/**
 * @file init.h
 * @author Dillon Buck, Samed Ozdemir, Matt Austen
 * @date 14 Apr 2014
 * @brief Initialize
 */

#ifndef INIT_H_
#define INIT_H_

/**
 * @brief Initializes everything
 */
void init(void);

#endif /* INIT_H_ */