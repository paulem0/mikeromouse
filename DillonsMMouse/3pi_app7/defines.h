/*
 * defines.h
 *
 * Created: 3/31/2014 11:26:02 PM
 *  Author: Dillon
 * 
 */ 

#include <stdbool.h>

#ifndef DEFINES_H_
#define DEFINES_H_

#define BLOCK 1752 /**< was 1840 */
#define TURN_ENCODER 652 /**< amount of ticks it takes to turn 90 degrees */
#define TURN_SPEED 35 /**< 15/128 speed */
#define SPEED_LEFT_MOTOR 50
#define SPEED_RIGHT_MOTOR 50
#define SPEED_CALIBRATION 20 /**< speed mouse is at in calibrate function */
#define IR_OFFSET_CALIBRATION_DISTANCE 10
#define IR_OFFSET_CALIBRATION_ROTATION 5
#define BLOCK_THRESHOLD 325 /**< minimum number of encoder ticks */
#define MOTOR_OFFSET 0
#define HIGH 1
#define LOW 0


volatile int8_t speedLeft;
volatile int8_t speedRight;
volatile uint16_t IR_LEFT_CENTER;
//volatile uint16_t IR_LEFT_FRONT_CENTER;
//volatile uint16_t IR_LEFT_BACK_CENTER;
volatile uint16_t IR_RIGHT_CENTER;
//volatile uint16_t IR_RIGHT_FRONT_CENTER;
//volatile uint16_t IR_RIGHT_BACK_CENTER;
volatile uint16_t IR_FRONT_CENTER;
volatile uint16_t IR_FRONT_LEFT_CENTER;
volatile uint16_t IR_FRONT_RIGHT_CENTER;
volatile uint16_t IR_LEFT_THRESHOLD;
volatile uint16_t IR_RIGHT_THRESHOLD;
volatile uint16_t IR_FRONT_THRESHOLD;
//volatile uint16_t IR_FRONT_LEFT_THRESHOLD;
//volatile uint16_t IR_FRONT_RIGHT_THRESHOLD;
volatile uint16_t IR_OFFSET_LEFT_RIGHT;
volatile uint16_t WALL_STOP;
volatile uint16_t leftSet;
volatile uint16_t rightSet;
volatile uint16_t frontSet;
volatile uint16_t frontLeftSet;
volatile uint16_t frontRightSet;
volatile uint16_t leftEncoder;
volatile uint16_t rightEncoder;
volatile uint16_t prev_ir_diff;
int8_t* path;
bool exploreMode;

#endif /* DEFINES_H_ */