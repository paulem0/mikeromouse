/*
 * motors.c
 *
 * Created: 3/31/2014 11:33:50 PM
 *  Author: Dillon
 */

#include <pololu/3pi.h>
#include <stdint.h>
#include "defines.h"
#include "init.h"
#include "motors.h"
#include "control.h"
#include "ir_sensors.h"

/*
// Turn to the direction that Chris algorithm tells us 0:no turn. 1: turn right. 2: turn around. 3: turn left
void turn(int8_t dir){
	reset_encoders();
	resetPID();
	switch(dir){
		case 0: // Go straight
		break;
		case 1: // Turn right 
			if(walls & (1 << 3) > 0){ // if there is a wall in front of you
				recalibrate2();
			}
			if (walls ^ (1 << 0) > 0){ // if there is a wall to your left
				// turn left
				set_motors(-TURN_SPEED, TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
				
				//recalibrate
				recalibrate2();
				
				// turn around and continue
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < 2 * TURN_ENCODER);
				set_motors(0,0);
			}else{
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
			}
		break;
		case 2: // Turn around
			if(walls & (1 << 3) > 0){ // if there is a wall in front of you
				recalibrate2();
			}
			if (walls & (1 << 0) > 0){ // if there is a wall to your left
				// turn left
				set_motors(-TURN_SPEED, TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
				
				//recalibrate
				recalibrate2();
				
				// turn left and continue
				set_motors(-TURN_SPEED, TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
			}else if(walls & (1 << 2) > 0){ // if there is a wall to your right
				// turn right
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
				
				//recalibrate
				recalibrate2();
				
				// turn right again
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
			}else{
				// turn around
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < 2 * TURN_ENCODER);
				set_motors(0,0);
			}
			break;
		case 3: // Turn left
			if(walls & (1 << 3) > 0){ // if there is a wall in front of you
				recalibrate2();
			}
			if (walls & (1 << 2) > 0){ // if there is a wall to your right
				// turn right
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
				
				//recalibrate
				recalibrate2();
				
				// turn around and continue
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < 2 * TURN_ENCODER);
				set_motors(0,0);
			}else{
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
			}
		break;
		default:
			red_led(1);
		break;
	}
	reset_encoders();
}*/

void turn(uint8_t dir){
	reset_encoders();
	resetPID();
	switch(dir){
		case 0: // Go straight
			break;
		case 1: // Turn right
		if(checkWalls() == 1){
			turn(3);
			recalibrate2();
			turn(2);
		}else{
			set_motors(TURN_SPEED, -TURN_SPEED);
			while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
			set_motors(0,0);
		}
			break;
		case 2: // Turn around
			if(checkWalls() == 1 || checkWalls() == 5){
				turn(3);
				recalibrate2();
				turn(3);
			}else if(checkWalls() == 4){
				turn(1);
				recalibrate2();
				turn(1);
			}else{
				// turn around
				set_motors(TURN_SPEED, -TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < 2 * TURN_ENCODER);
				set_motors(0,0);
			}
			break;
		case 3: // Turn left
			if(checkWalls() == 4){
				turn(1);
				recalibrate2();
				turn(2);
			}else{
				set_motors(-TURN_SPEED, TURN_SPEED);
				while((leftEncoder+rightEncoder)/2 < TURN_ENCODER);
				set_motors(0,0);
			}
			break;
		default:
			red_led(1);
			break;
	}
	reset_encoders();
}

// Move forward ~1 block while using a PID control function to keep the micro mouse on track and avoiding walls.
void move(){
	reset_encoders();
	set_motors(SPEED_LEFT_MOTOR, SPEED_RIGHT_MOTOR);
	while((leftEncoder+rightEncoder)/2 < BLOCK){
		// Anthony's request (turn off the PID when the mouse is about to come to a wall peg)
		
		if((leftEncoder+rightEncoder)/2 < ((BLOCK / 2) - BLOCK_THRESHOLD)){ // while the micro-mouse is within the first ~200 encoders ticks per block, turn PID on.
			PID2();
			clear();
			print("ON");
		}else if((leftEncoder+rightEncoder)/2 > ((BLOCK / 2) + BLOCK_THRESHOLD)){ // after the micro-mouse has passed the peg, turn PID on
			PID2();
			clear();
			print("ON");
		}else{
			//normalizeMotors();
			clear();
			print("OFF");
			set_motors(SPEED_LEFT_MOTOR, SPEED_RIGHT_MOTOR); // while the micro-mouse is near a peg, turn the PID off
		}
		//PID2();
	}
	set_motors(0,0);
}

// reset the values of the encoders to 0
void reset_encoders(void){
	leftEncoder = 0;
	rightEncoder = 0;
}

void normalizeMotors(void){
	// accelerate motors to try and avoid jerking when there are no walls on either side.
	while(speedLeft != SPEED_LEFT_MOTOR && speedRight != SPEED_RIGHT_MOTOR){
		if(speedLeft < SPEED_LEFT_MOTOR){
			speedLeft++;
		}else if(speedLeft > SPEED_LEFT_MOTOR){
			speedLeft--;
		}
		if(speedRight < SPEED_RIGHT_MOTOR){
			speedRight++;
		}else if(speedRight > SPEED_RIGHT_MOTOR){
			speedRight--;
		}
	}
}