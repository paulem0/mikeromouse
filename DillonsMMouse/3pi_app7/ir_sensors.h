/**
* @file ir_sensors.h
* @author Dillon Buck, Samed Ozdemir, Matt Austen
* @date 14 Apr 2014
* @brief File containing IR sensor functions
*/


#ifndef IR_SENSORS_H_
#define IR_SENSORS_H_

/**
* @brief
* Averages the IR sensors on both sides (left and right) to determine the center of the block
*/
 uint16_t avg_sides();

/**
* @brief 
* Check left, front, and right side of micro-mouse for a wall. 
* If there is a wall there, set the bit high. The wall behind the micro-mouse is assumed to be 0 at all times.
* Returns uint8_t containing wall information in the form of XXXXURDL (up, right, down, left).
*/
uint8_t checkWalls();


/**
* @brief 
* Used for debugging sensors on the left and right side (message is printed to screen)
*/
void debugLR();

/**
* @brief 
* Used for debugging the sensors in the front of the micro-mouse (message is printed to screen)
*/
void debugF();

/**
* @brief 
* Used for debugging all the sensors (message is printed to screen)
* depreciated (more sensors were added making this function less useful)
*/
void debugSensors();

/**
* @brief 
* Read and return the value of the front left IR sensor
*/
uint16_t readFrontLeft(void);

/**
* @brief 
* Read and return the value of the front right IR sensor
*/
uint16_t readFrontRight(void);

/**
* @brief 
* Read both front IR sensors and return the average value
*/
uint16_t readFront(void);

/**
* @brief 
* Read the left IR sensor closer to the front
*/
uint16_t readLeftFront(void);

/**
* @brief 
* Read the left IR sensor closer to the back
*/
uint16_t readLeftBack(void);

/**
* @brief 
* Read and return the average values of the left IR sensors
*/
uint16_t readLeft(void);

/**
* @brief 
* Read and return the average value of the right IR sensors
*/
uint16_t readRightFront(void);

/**
* @brief 
* Read and return the average value of the right IR sensors
*/
uint16_t readRightBack(void);

/**
* @brief 
* Record the left IR sensor closer to the front when the micro-mouse is in the middle of the block at start.
*/
uint16_t seedLeftFront(void);

/**
* @brief 
* Record the left IR sensor closer to the back when the micro-mouse is in the middle of the block at start.
*/
uint16_t seedLeftBack(void);

/**
* @brief 
* Record the right IR sensor closer to the front when the micro-mouse is in the middle of the block at start.
*/
uint16_t seedRightFront(void);

/**
* @brief 
* Record the right IR sensor closer to the back when the micro-mouse is in the middle of the block at start.
*/
uint16_t seedRightBack(void);

/**
* @brief  
* Record the front left IR sensor when the micro-mouse is in the middle of the block at start.
*/
uint16_t seedFrontLeft(void);

/**
* @brief  
* Record the front left IR sensor when the micro-mouse is in the middle of the block at start.
*/
uint16_t seedFrontRight(void);

/**
* @brief  
* Record the average value of both front IR sensors.
*/
uint16_t seedFront(void);

/**
* @brief  
* Checks for the difference in the IR sensors on the left and right side
*/
int checkDistance(void);

#endif /* IR_SENSORS_H_ */