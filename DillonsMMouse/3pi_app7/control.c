/*
 * control.c
 *
 * Created: 4/1/2014 1:54:33 AM
 *  Author: Dillon
 */ 
#include <pololu/3pi.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "defines.h"
#include "ir_sensors.h"
#include "init.h"

uint16_t left;
uint16_t right;
uint16_t front;
uint8_t SETPOINT = 0;

int ir_diff = 0;
int error = 0;
int prevError = 0;
float pid = 0;

float dt = 0;
float ticks = 0;
float prevTicks = 0;
float accum_error = 0;
float xP = 0;
float xI = 0;
float xD = 0;

float Kp = 12.5; // 12.5
float Ki = 5; // 5
float Kd = 12.5; // 12.5
bool select = true;

//reset PID
void resetPID(){
	accum_error = 0;
}

// Another PID function using IR sensors
void PID2(){
	front = readFront();
	
	if(front > WALL_STOP){ // If there is a wall, stop. Hopefully we never enter into this
		set_motors(0,0);
		leftEncoder = BLOCK+100;
		rightEncoder = BLOCK+100;
	}else{
		delay(15);	
			// check distance to walls on left and right side
			ir_diff = checkDistance();
			// calculate the error
			error = 0 - ir_diff;
			
		if(ir_diff != 0 || ir_diff != 65000){ // if there is at least one wall OR not an jitter in calculating error
			// Calculate PID 
			
			// Calculate dt
			ticks = get_ticks();
			dt = ticks_to_microseconds(ticks - prevTicks) / 1000000.0;
			prevTicks = ticks;

			// Calculate P term
			xP = Kp * error;
		
			// Calculate I term
			accum_error += (error * dt);
			xI = Ki * accum_error;

			// Calculate the D
			xD = Kd * ((error - prevError) / dt);
		
			prevError = error;
		
			pid = ((xP + xI + xD) / 1000.0);
			
			//update motors
			speedLeft = SPEED_LEFT_MOTOR - pid / 2;
			speedRight = SPEED_RIGHT_MOTOR + pid / 2;
			
			set_motors(speedLeft, speedRight);
		}else{
			//normalizeMotors();
			set_motors(SPEED_LEFT_MOTOR - MOTOR_OFFSET, SPEED_RIGHT_MOTOR + MOTOR_OFFSET);
		}
	}
}

// when there is a wall in front of you, re-calibrate the micro-mouses distance
void recalibrate(uint8_t walls){
	//walls &= (1 << 3); // make it so that the mouse only calibrates with the front wall (temporary) Take this out if you want to correct position with all walls
	
	// switch case for each possible wall condition
	// only front wall
	// only left wall
	// only right wall
	// only front and right wall
	// only front and left wall
	// only left and right wall
	// front, left, and right
	
	switch(walls){
		/*case 1:
			turn(3);
			recalibrate2();
			turn(1);
			break;
		case 4:
			turn(1);
			recalibrate2();
			turn(3);
			break;
		case 5:
			turn(1);
			recalibrate2();
			turn(3);
			break;*/
		case 8:
			recalibrate2();
			break;
		case 9:
			recalibrate2();
			turn(3);
			recalibrate2();
			turn(1);
			recalibrate2();
			break;
		case 12:
			recalibrate2();
			turn(1);
			recalibrate2();
			turn(3);
			recalibrate2();
			break;
		case 13:
			recalibrate2();
			if(readLeftBack() < readRightBack()){
				turn(3);
				recalibrate2();
				turn(1);
				recalibrate2();
			}else{
				turn(1);
				recalibrate2();
				turn(3);
				recalibrate2();	
			}
			break;
		default:
			break;
	}
}

void recalibrate2(void){
	// If the difference between the sensors is greater than the threshold, we need to calibrate
	while( abs(readFrontLeft() - readFrontRight()) > IR_OFFSET_CALIBRATION_ROTATION){
		
		// If the left is closer than the right, we need to turn CCW
		if(readFrontLeft() >= readFrontRight()){
			
			set_motors(-SPEED_CALIBRATION, SPEED_CALIBRATION);
			// Move until the difference is satisfied
			while(abs(readFrontLeft() - readFrontRight()) > IR_OFFSET_CALIBRATION_ROTATION){
			}
			
			// Turn the motors off
			set_motors(0, 0);

		}
		else{
			
			set_motors(SPEED_CALIBRATION, -SPEED_CALIBRATION);
			
			while(abs(readFrontLeft() - readFrontRight()) > IR_OFFSET_CALIBRATION_ROTATION){
			}
			
			// Turn the motors off
			set_motors(0, 0);
			
		}
	}
	delay(20);
	
	//recalibrate again
	// If the difference between the sensors is greater than the threshold, we need to calibrate
	while( abs(readFrontLeft() - readFrontRight()) > IR_OFFSET_CALIBRATION_ROTATION){
		
		// If the left is closer than the right, we need to turn CCW
		if(readFrontLeft() >= readFrontRight()){
			
			set_motors(-SPEED_CALIBRATION, SPEED_CALIBRATION);
			// Move until the difference is satisfied
			while(abs(readFrontLeft() - readFrontRight()) > IR_OFFSET_CALIBRATION_ROTATION){
			}
			
			// Turn the motors off
			set_motors(0, 0);

		}
		else{
			
			set_motors(SPEED_CALIBRATION, -SPEED_CALIBRATION);
			
			while(abs(readFrontLeft() - readFrontRight()) > IR_OFFSET_CALIBRATION_ROTATION){
			}
			
			// Turn the motors off
			set_motors(0, 0);
			
		}

	}
	
	// Corrects position
	while(readFront() < (IR_FRONT_CENTER - IR_OFFSET_CALIBRATION_DISTANCE)){	// move close to wall if the micro mouse is to far away
		set_motors(SPEED_CALIBRATION, SPEED_CALIBRATION);
	}
	while(readFront() > (IR_FRONT_CENTER + IR_OFFSET_CALIBRATION_DISTANCE)){	//back up if wall is to close
		set_motors(-SPEED_CALIBRATION, -SPEED_CALIBRATION);
	}
	set_motors(0,0);
	
	reset_encoders();
}