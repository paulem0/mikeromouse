#include <pololu/3pi.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdbool.h>
#include <avr/wdt.h>
#include "defines.h"
#include "ir_sensors.h"
#include "motors.h"
#include "init.h"
#include "control.h"
#include "maze_solver.h"

// TODO:
// Fix the mouse so that when it starts in the bottom right side of the maze, it can solve properly.
// Put in a 'going home' function

int main(void) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
	
	wdt_disable(); // Disable the watchdog timer
	
	int8_t dir = 0;		// dir stores direction to turn that is retrieved from Chris's algorithm. 0: straight, 1: right, 2: back, 3: left.
	uint8_t walls = 0;	// Variable to store data grabbed by the the checkWalls function. Stored as a binary value XXXXURDL where 1 indicates a wall 
	uint8_t tempWalls = 0; // variable to hold the tempWall information used in determining which side of the mouse the maze is on at the start
	uint8_t starting_point_x = 0; // Starting position of the mouse in the maze. Defaulted to 0,0 but if the mouse finds the maze is on it's left, the mouse resets and starting_x is assigned to 16 making starting position 16,0.
	bool first_run = true; // tells the micro mouse whether or not it is on it's first run through the main loop to avoid recalibrating at start
	bool mazeLocationFound = false; // boolean used to tell the mouse whether the maze location (left or right) is found or whether it should keep checking
	bool ff = true; // Floodfill algorithm is used to determine next move until the end point (center) is found. Once found, ff is set false and DFS is then used to find next move.
	uint8_t ending_x = 0;
	uint8_t ending_y = 0;
	
	// Initialize registers as inputs with pull up resistors
	init();
	
	lcd_init_printf();
	
	// Wait till button B is pressed before continuing
	while(!button_is_pressed(BUTTON_B)){
		lcd_goto_xy(0,0);
		printf("X: %d", ending_x);
		lcd_goto_xy(0,1);
		printf("Y: %d", ending_y);
		if(button_is_pressed(BUTTON_A)){
			delay(300);
			ending_x++;
			if(ending_x > 3){
				ending_x = 0;
			}
		}
		if(button_is_pressed(BUTTON_C)){
			delay(300);
			ending_y++;
			if(ending_y > 3){
				ending_y = 0;
			}
		}
	};
	
	delay(1500); // delay 1.5 seconds before doing anything (allows user to get out of the way before seeding the sensors).
	
	// initialize the maze in memory
	maze_solver_init();
	maze_init_ff();
	
	// set starting position to be (0, 0) (aka. bottom left) by default
	maze_set_start_point(0, 0);
	
	// set starting position facing south of maze (a wall on the left, right, and in front of mouse).
	maze_set_start_rotation(180); 
	
	// set the wall threshold on the back IR sensors of each side
	leftSet = seedLeftBack();
	rightSet = seedRightBack();
	
	// set up the thresholds for the front sensors that are used to calibrate the micro mouse
	frontLeftSet = seedFrontLeft();
	frontRightSet = seedFrontRight();
	frontSet = seedFront();
	
	// seed the left and right IRs
	//IR_LEFT_FRONT_CENTER = seedLeftFront();
	//IR_LEFT_BACK_CENTER = seedLeftBack();
	//IR_RIGHT_FRONT_CENTER = seedRightFront();
	//IR_RIGHT_BACK_CENTER = seedRightBack();
	
	IR_LEFT_CENTER = leftSet; // determine value left IR sensor should read when it's at the center of a block
	IR_LEFT_THRESHOLD = leftSet / 4; // determine the minimum value that the left IR sensor should read when it sees a wall
	
	IR_RIGHT_CENTER = rightSet; // determine value right IR sensor should read when it's at the center of a block
	IR_RIGHT_THRESHOLD = rightSet / 4; // determine the minimum value that the right IR sensor should read when it sees a wall
		
	IR_FRONT_LEFT_CENTER = frontLeftSet; // determine the value front IR sensor should read when it's at the center of a block
	//IR_FRONT_LEFT_THRESHOLD = frontLeftSet / 4;
	
	IR_FRONT_RIGHT_CENTER = frontRightSet; // determine value front IR sensor should read when it's at the center of a block
	//IR_FRONT_RIGHT_THRESHOLD = frontRightSet / 4;
	
	IR_FRONT_CENTER = frontSet; // determine the average value the front sensors should read when the micro mouse is in the center of a block
	IR_FRONT_THRESHOLD = frontSet / 4; // determine the value minimum value the front IR sensors should read when it reads a wall
	
	WALL_STOP = frontSet / 2; // determine the minimum value the front IR sensors need to read before stopping for a wall
	IR_OFFSET_LEFT_RIGHT = IR_LEFT_CENTER - IR_RIGHT_CENTER; // offset between left and right sensors
	
	// Mouse is ready to start exploring the maze
	while(dir != -1) {
		
		while(!mazeLocationFound){ // while the location of the maze is unknown, 
			walls = checkWalls(); // get information about walls. binary number in the form XXXXURDL where 1 indicates a wall
			
			// only take information about whether there is a wall on the left or right
			tempWalls = walls & 5; // mask all unwanted bits
			tempWalls ^= 5; // XOR with 00000101. if ans > 1, then either the left or right wall is missing and the maze location can be found
			
			if(!first_run){ // don't calibrate the first time around
				recalibrate(walls); // recalibrate the mouse if it can
			}
			
			first_run = false;
			
			if(tempWalls == 1){ // if there is no wall on the left side of the mouse, reset the mouse and maze and initialize it properly.
				turn(2); // turn mouse around
				while(readFront() < IR_FRONT_THRESHOLD){
					move(); // go back to the starting position
				}
				recalibrate2(); // recalibrate against the walls
				maze_clear(); // clear maze memory
				starting_point_x = 16; // set starting position to 16,0
				maze_set_start_point(starting_point_x, 0);
				maze_set_start_rotation(180); // set orientation to 180 degrees
				mazeLocationFound = true; // tell the mouse the maze location has been found!
			}else if(tempWalls == 4){ // if a wall on the right is missing, our initial settings (maze or right with starting position at 0,0) was correct
				mazeLocationFound = true; // tell the mouse we have found location of the maze
			}else{ // keep looking for the location of the maze while moving along the first column searching for a missing wall
				maze_update_node(walls);
				
				dir = maze_next_direction_ff();
				
				turn(dir);
				
				move();
				
			}
			
		} 
		
		// walls = 0;
		// dir = 0;
		
		walls = checkWalls(); // Retrieve wall information at current node
		
		if(!first_run){ // don't calibrate the first time around
			recalibrate(walls); // recalibrate the mouse if it can
		}
		
		walls = checkWalls(); // Retrieve wall information at current node
		
		maze_update_node(walls);  // update the maze in memory
		
		if(ff){ // if floodfill has not yet found the center of the maze
			dir = maze_next_direction_ff(); // determine which direction to turn using floodfill method
			if(dir == -1){ // if floodfill method has found the center
				clear(); // clear the lcd screen
				//print("FF solved");
				set_motors(0,0); // stop the motors
				//delay(2500);
				ff = false; // tell the mouse he should switch to using DFS 
				
				walls = checkWalls(); // Retrieve wall information at current node
				
				recalibrate(walls); // recalibrate the mouse if it can
				
				walls = checkWalls(); // Retrieve wall information at current node (double check after recalibration)
				
				maze_update_node(walls);  // update the maze in memory
				
				dir = maze_next_direction_dfs(); // determine which direction to turn using DFS
			}
		}else{
			dir = maze_next_direction_dfs(); // determine which direction to turn using DFS
		}
		
		if((dir == -1) && !ff){ // if DFS returns a -1 (traversed the whole maze)
			clear();
			print("Calc'ing");
			set_motors(0,0);
			delay(2500);
			exploreMode = false; // // we are done exploring the maze
			path = maze_dijkstras_algorithm(starting_point_x, 0, ending_x, ending_y); // find the quickest path from starting position to 8,8
			clear();
			print("Solving");
		}else{ // otherwise continue exploring
			turn(dir); 
			move(); // Move forward one block
		}
				
		while((dir == -1) && !ff){  // solve the maze according to the path returned by the Dijkstra algorithm
			clear();
			walls = checkWalls();
			recalibrate(walls);			
			print(*path);		
			while(*path == -1){ // when the mouse reaches the center
				clear();
				print("Solved!!");
				green_led(1);
				delay(5000);
			}
			green_led(1);
			turn(*path);
			path++;
			move();
		}
	}	
}

// This interrupt service routine is for the channel connected to PB2
ISR(PCINT0_vect) {
	rightEncoder++;
}

// This interrupt service routine is for the channel connected to PD0
ISR(PCINT2_vect) {
	leftEncoder++;
}